package org.terna.ehrp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.terna.ehrp.Common.Common;
import org.terna.ehrp.Model.Doctor;

public class DoctorSignIn extends AppCompatActivity {

    EditText phonenumber,password;
    Button dsignin,dsignup;

    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_sign_in);

        phonenumber=(MaterialEditText)findViewById(R.id.phonenumber);
        password=(MaterialEditText)findViewById(R.id.password);

        dsignin=(Button)findViewById(R.id.dsignin);
        dsignup=(Button)findViewById(R.id.dsignup);

        firebaseDatabase = FirebaseDatabase.getInstance();

        final DatabaseReference databaseReference = firebaseDatabase.getReference("Doctor");

        dsignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog mDialog = new ProgressDialog(DoctorSignIn.this);
                mDialog.setMessage("Please Wait..");
                mDialog.show();

                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.child(phonenumber.getText().toString()).exists()){
                            mDialog.dismiss();
                            Doctor user = dataSnapshot.child(phonenumber.getText().toString()).getValue(Doctor.class);
                            user.setPhonenumber(phonenumber.getText().toString());
                            if (user.getPassword().equals(password.getText().toString())) {
                                Toast.makeText(DoctorSignIn.this, "Sign in successfully", Toast.LENGTH_SHORT).show();
                                Intent homeIntent = new Intent(DoctorSignIn.this,DoctorDashboard.class);
                                Common.currentuser = user;
                                startActivity(homeIntent);
                                finish();
                            } else {
                                Toast.makeText(DoctorSignIn.this, "Incorrect Password!", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        dsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DoctorSignIn.this,DoctorSignUp.class));
            }
        });





    }
}
