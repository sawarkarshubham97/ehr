package org.terna.ehrp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.terna.ehrp.Model.BloodPressureModel;

import java.util.ArrayList;
import java.util.List;

public class BloodPressure extends AppCompatActivity {

    TextView systolic,diastolic,date;
    Button save;
    FirebaseDatabase database;
    DatabaseReference databaseReference;

    private RecyclerView mRecyclerView;
    private BloodPressureAdapter mAdapter;
    FirebaseAuth firebaseAuth;
    private List<BloodPressureModel> mUploads;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_pressure);

        systolic=(TextView)findViewById(R.id.systolic);
        diastolic=(TextView)findViewById(R.id.diastolic);
        date=(TextView)findViewById(R.id.date);
        save=(Button)findViewById(R.id.save);

        mRecyclerView=findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mUploads = new ArrayList<>();


        FloatingActionButton fab =(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog();
            }
        });

        firebaseAuth=FirebaseAuth.getInstance();
        database=FirebaseDatabase.getInstance();

        databaseReference = database.getReference("Patient").child(firebaseAuth.getCurrentUser().getUid()).child("Blood Pressure");


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    BloodPressureModel bloodPressureModel =postSnapshot.getValue(BloodPressureModel.class);
                    bloodPressureModel.setKey(postSnapshot.getKey());
                    mUploads.add(bloodPressureModel);

                }

                mAdapter=new BloodPressureAdapter(BloodPressure.this,mUploads);

                mRecyclerView.setAdapter(mAdapter);



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(BloodPressure.this,databaseError.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });



    }

    private void showDialog() {
        AlertDialog.Builder alertDialog= new AlertDialog.Builder(BloodPressure.this);


        LayoutInflater inflater = this.getLayoutInflater();
        View add_record = inflater.inflate(R.layout.blood_pressure,null);

        systolic=add_record.findViewById(R.id.systolic);
        diastolic=add_record.findViewById(R.id.diastolic);
        date=add_record.findViewById(R.id.date);
        save=add_record.findViewById(R.id.save);

        databaseReference = database.getReference("Patient").child(firebaseAuth.getCurrentUser().getUid()).child("Blood Pressure");


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BloodPressureModel bloodPressure= new org.terna.ehrp.Model.BloodPressureModel(systolic.getText().toString(),diastolic.getText().toString(),date.getText().toString());


                String uploadId = databaseReference.push().getKey();
                databaseReference.child(uploadId).setValue(bloodPressure);

                finish();


            }
        });
        alertDialog.setView(add_record);
        alertDialog.show();


    }
}
