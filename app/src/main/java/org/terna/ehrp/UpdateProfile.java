package org.terna.ehrp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.terna.ehrp.Model.Patient;

public class UpdateProfile extends AppCompatActivity {

    EditText name, email,edtPassword, number, age, address, gender;
    Button save;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase database;
    RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        name = (MaterialEditText) findViewById(R.id.name);
        email = (MaterialEditText) findViewById(R.id.email);
        number = (MaterialEditText) findViewById(R.id.number);
        age = (MaterialEditText) findViewById(R.id.age);
        address = (MaterialEditText) findViewById(R.id.address);
        gender = (MaterialEditText) findViewById(R.id.gender);
        layout = (RelativeLayout) findViewById(R.id.layout);

        save = (Button) findViewById(R.id.save);


        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        final DatabaseReference databaseReference = database.getReference("Patient").child(firebaseAuth.getCurrentUser().getUid()).child("Patient Profile");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Patient user = dataSnapshot.getValue(Patient.class);
                name.setText(user.getPname());
                email.setText(user.getPemail());
                number.setText(user.getPhone());
                age.setText(user.getPage());
                gender.setText(user.getPgender());
                address.setText(user.getPaddress());




            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(UpdateProfile.this, databaseError.getCode(), Toast.LENGTH_SHORT).show();

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pname = name.getText().toString();
                String pemail = email.getText().toString();
                String phone = number.getText().toString();
                String page = age.getText().toString();
                String pgender = gender.getText().toString();
                String paddress = address.getText().toString();


                Patient user = new Patient(pname, pemail, phone, page,pgender,paddress);

                databaseReference.setValue(user);

                finish();

            }
        });



    }
}
