package org.terna.ehrp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class Appointment extends AppCompatActivity  {

    CardView bookappointment,viewappointment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        bookappointment =(CardView) findViewById(R.id.bookappointment);
        viewappointment =(CardView)findViewById(R.id.viewappointment);

        bookappointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Appointment.this, DoctorList.class));
            }
        });
        viewappointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Appointment.this, BookDoctorAppointment.class));
            }
        });

    }


}
