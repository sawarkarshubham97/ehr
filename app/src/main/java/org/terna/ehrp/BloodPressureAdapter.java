package org.terna.ehrp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.terna.ehrp.Model.BloodPressureModel;

import java.util.List;

public class BloodPressureAdapter extends RecyclerView.Adapter<BloodPressureAdapter.ViewHolder> {

    private Context mContext;
    private List<BloodPressureModel> mUploads;

    public BloodPressureAdapter(Context context,List<BloodPressureModel> uploads){

        mContext=context;
        mUploads=uploads;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.blood_pressure_list,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        BloodPressureModel bloodPressureModel = mUploads.get(position);

        holder.systolic.setText(bloodPressureModel.getSystolic());
        holder.diastolic.setText(bloodPressureModel.getDiastolic());
        holder.date.setText(bloodPressureModel.getDate());


    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView systolic,diastolic,date;

        public ViewHolder(View itemView) {
            super(itemView);

            systolic = itemView.findViewById(R.id.systolic);
            diastolic = itemView.findViewById(R.id.diastolic);
            date = itemView.findViewById(R.id.date);

        }
    }
}
