package org.terna.ehrp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.terna.ehrp.Model.Doctor;

import java.util.ArrayList;
import java.util.List;

public class DoctorList extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private DoctorListAdapter mAdapter;

    private DatabaseReference mDatabaseref;
    private List<Doctor> mUploads;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);

        mRecyclerView=findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mUploads = new ArrayList<>();

        mDatabaseref= FirebaseDatabase.getInstance().getReference("Doctor").child("Doctor Profile");

        mDatabaseref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    Doctor doctor =postSnapshot.getValue(Doctor.class);
                    mUploads.add(doctor);

                }

                mAdapter=new DoctorListAdapter(DoctorList.this,mUploads);

                mRecyclerView.setAdapter(mAdapter);



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(DoctorList.this,databaseError.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });
    }
}
