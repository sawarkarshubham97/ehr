package org.terna.ehrp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.terna.ehrp.Model.Patient;

public class Profile extends AppCompatActivity {


    private String email;
    private TextView txtname,txtnumber,txtemail,txtage,txtgender,txtaddress;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    Button edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtname=(TextView)findViewById(R.id.txtname);
        txtnumber = (TextView)findViewById(R.id.txtnumber);
        txtemail = (TextView)findViewById(R.id.txtemail);
        txtage = (TextView)findViewById(R.id.txtage);
        txtgender = (TextView)findViewById(R.id.txtgender);
        txtaddress = (TextView)findViewById(R.id.txtaddress);
        edit=(Button)findViewById(R.id.edit);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();


            DatabaseReference databaseReference = firebaseDatabase.getReference("Patient").child(firebaseAuth.getCurrentUser().getUid()).child("Patient Profile");

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                    Patient user = dataSnapshot.getValue(Patient.class);
                    txtname.setText("Name: " +user.getPname());
                    txtemail.setText("Email: " +user.getPemail());
                    txtnumber.setText("Mobile Number: " +user.getPhone());
                    txtage.setText("Age: " +user.getPage());
                    txtgender.setText("Gender: " +user.getPgender());
                    txtaddress.setText("City: " +user.getPaddress());



                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(Profile.this, databaseError.getCode(), Toast.LENGTH_SHORT).show();

                }
            });

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Profile.this, UpdateProfile.class));
                }
            });


    }
}
