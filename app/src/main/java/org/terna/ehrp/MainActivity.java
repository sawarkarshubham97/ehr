package org.terna.ehrp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.rengwuxian.materialedittext.MaterialEditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText email,edtPassword;
    Button signin,signup,forgot_password;
    FirebaseAuth firebaseAuth;
    RelativeLayout activity_main;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //View
        email = (MaterialEditText) findViewById(R.id.email);
        edtPassword= (MaterialEditText) findViewById(R.id.edtPassword);

        signin = (Button) findViewById(R.id.signin);
        signup = (Button) findViewById(R.id.signup);
        forgot_password=(Button)findViewById(R.id.forgot_password);

        activity_main =(RelativeLayout)findViewById(R.id.activity_main);

        signin.setOnClickListener(this);
        signup.setOnClickListener(this);
        forgot_password.setOnClickListener(this);

        //Initialize Firebase Auth
         firebaseAuth = FirebaseAuth.getInstance();

         //check already session, if ok -Home
        if(firebaseAuth.getCurrentUser() != null){
            startActivity(new Intent(MainActivity.this,Home.class));
        }

    }


    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.forgot_password){
            startActivity(new Intent(MainActivity.this,DoctorSignIn.class));
        }
        else if(v.getId()==R.id.signup){
            startActivity(new Intent(MainActivity.this,Signup.class));
            finish();
        }
        else if(v.getId()==R.id.signin){
            loginuser(email.getText().toString(),edtPassword.getText().toString());

        }

    }

    private void loginuser(String email, final String password) {
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful())
                {
                    if(password.length() < 6)
                    {
                        Snackbar snackbar = Snackbar.make(activity_main,"Password Length should be over 6",Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                }
                else{
                    startActivity(new Intent(MainActivity.this,Home.class));
                }
            }
        });

    }
}
