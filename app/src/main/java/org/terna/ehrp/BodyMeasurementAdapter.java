package org.terna.ehrp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.terna.ehrp.Model.BodyMeasurementModel;

import java.util.List;

public class BodyMeasurementAdapter extends RecyclerView.Adapter<BodyMeasurementAdapter.ViewHolder> {

    private Context mContext;
    private List<BodyMeasurementModel>mUploads;

    public BodyMeasurementAdapter(Context context,List<BodyMeasurementModel> uploads){

        mContext=context;
        mUploads=uploads;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.body_measurement_list,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BodyMeasurementModel bodyMeasurementModel = mUploads.get(position);
        holder.height.setText(bodyMeasurementModel.getHeight());
        holder.weight.setText(bodyMeasurementModel.getWeight());
        holder.date.setText(bodyMeasurementModel.getDate());

    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView height,weight,date;

        public ViewHolder(View itemView) {
            super(itemView);

            height=itemView.findViewById(R.id.height);
            weight=itemView.findViewById(R.id.weight);
            date=itemView.findViewById(R.id.date);

        }

    }
}
