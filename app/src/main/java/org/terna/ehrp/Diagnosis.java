package org.terna.ehrp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class Diagnosis extends AppCompatActivity {

    CardView bloodpressure,sugarlevel,body;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnosis);

        bloodpressure=(CardView)findViewById(R.id.bloodpressure);
        sugarlevel=(CardView)findViewById(R.id.sugarlevel);
        body=(CardView)findViewById(R.id.body);

        bloodpressure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Diagnosis.this, BloodPressure.class));
            }
        });
        sugarlevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Diagnosis.this, SugarLevel.class));
            }
        });
        body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Diagnosis.this, BodyMeasurement.class));

            }
        });
    }
}
