package org.terna.ehrp;

import android.view.View;

interface ItemClickListner {

    void  onClick(View view, int position, boolean isLongClick);
}
