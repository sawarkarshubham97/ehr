package org.terna.ehrp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.terna.ehrp.Model.Doctor;

import java.util.List;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.ViewHolder> {

    private Context mContext;
    private List<Doctor> mUploads;



    public DoctorListAdapter(Context context,List<Doctor> uploads){

        mContext=context;
        mUploads=uploads;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         View v = LayoutInflater.from(mContext).inflate(R.layout.doctor_list,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Doctor doctor =mUploads.get(position);
        holder.dname.setText(doctor.getDname());
        holder.hospitalname.setText(doctor.getHospitalname());
        holder.specialization.setText(doctor.getSpecialization());
    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView dname,hospitalname,specialization;

        private ItemClickListner itemClickListner;


        public ViewHolder(View itemView) {
            super(itemView);

            dname = itemView.findViewById(R.id.dname);
            hospitalname = itemView.findViewById(R.id.hospitalname);
            specialization = itemView.findViewById(R.id.specialization);

            itemView.setOnClickListener(this);

        }

        public void setItemClickListner(ItemClickListner itemClickListner) {
            this.itemClickListner = itemClickListner;
        }


        @Override
        public void onClick(View view) {
            itemClickListner.onClick(view,getAdapterPosition(),false);
        }
    }
}
