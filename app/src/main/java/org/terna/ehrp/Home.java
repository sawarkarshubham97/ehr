package org.terna.ehrp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private CardView appointment,record,profile,reminder,diagnosis;
    TextView txtFullName;
    MenuItem nav_profile,nav_appointment,nav_record,nav_reminder,nav_change_password,nav_logout;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        profile = (CardView) findViewById(R.id.profile);
        appointment = (CardView)findViewById(R.id.appointment);
        record = (CardView)findViewById(R.id.record);
        reminder = (CardView) findViewById(R.id.reminder);
        diagnosis = (CardView) findViewById(R.id.diagnosis);
        nav_logout=(MenuItem)findViewById(R.id.nav_logout);


        profile.setOnClickListener(this);
        appointment.setOnClickListener(this);
        record.setOnClickListener(this);
        reminder.setOnClickListener(this);
        diagnosis.setOnClickListener(this);

        firebaseAuth=FirebaseAuth.getInstance();
        firebaseDatabase=FirebaseDatabase.getInstance();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(firebaseAuth.getCurrentUser() != null){
            View headerView = navigationView.getHeaderView(0);
            txtFullName = (TextView) headerView.findViewById(R.id.txtFullName);
            txtFullName.setText("Hello,"+firebaseAuth.getCurrentUser().getEmail());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent i;
            i= new Intent(Home.this,Profile.class);
            startActivity(i);

            // Handle the camera action
        } else if (id == R.id.nav_appointment) {
            Intent i;
            i= new Intent(Home.this,Appointment.class);
            startActivity(i);


        } else if (id == R.id.nav_record) {
            Intent i;
            i= new Intent(Home.this,Record.class);
            startActivity(i);

        } else if (id == R.id.nav_reminder) {
            Intent i;
            i= new Intent(Home.this,Reminder.class);
            startActivity(i);

        }
        else if (id == R.id.nav_change_password) {
            Intent i;
            i= new Intent(Home.this,Reminder.class);
            startActivity(i);

        }
        else if (id == R.id.nav_logout) {
            logoutUser();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutUser() {
        firebaseAuth.signOut();
        if(firebaseAuth.getCurrentUser() == null){
            startActivity(new Intent(Home.this,MainActivity.class));
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()){

            case R.id.profile:
                i= new Intent(Home.this,Profile.class);
                startActivity(i);
                break;
            case R.id.appointment:
                i= new Intent(Home.this,Appointment.class);
                startActivity(i);
                break;
            case R.id.record:
                i= new Intent(Home.this,Record.class);
                startActivity(i);
                break;
            case R.id.reminder:
                i= new Intent(Home.this,Reminder.class);
                startActivity(i);
                break;
            case R.id.diagnosis:
                i= new Intent(Home.this,Diagnosis.class);
                startActivity(i);
                break;



            default:
                break;
        }

    }
}
