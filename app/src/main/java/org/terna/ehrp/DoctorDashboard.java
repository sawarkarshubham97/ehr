package org.terna.ehrp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class DoctorDashboard extends AppCompatActivity implements View.OnClickListener {
    CardView profile,appointment,patient,aboutus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_dashboard);

        profile =(CardView)findViewById(R.id.profile);
        appointment=(CardView)findViewById(R.id.appointment);
        patient =(CardView)findViewById(R.id.patient);
        aboutus =(CardView)findViewById(R.id.aboutus);

        profile.setOnClickListener(this);
        appointment.setOnClickListener(this);
        patient.setOnClickListener(this);
        aboutus.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {


    }
}
