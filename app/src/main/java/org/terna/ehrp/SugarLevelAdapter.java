package org.terna.ehrp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.terna.ehrp.Model.SugarLevelModel;

import java.util.List;

public class SugarLevelAdapter extends RecyclerView.Adapter<SugarLevelAdapter.ViewHolder> {

    private Context mContext;
    private List<SugarLevelModel> mUploads;
     public SugarLevelAdapter(Context context,List<SugarLevelModel>uploads){

         mContext=context;
         mUploads=uploads;
     }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.sugar_level_list,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

         SugarLevelModel sugarLevelModel = mUploads.get(position);

         holder.fasting.setText(sugarLevelModel.getFasting());
         holder.random.setText(sugarLevelModel.getRandom());
         holder.date.setText(sugarLevelModel.getDate());

    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView fasting,random,date;


        public ViewHolder(View itemView) {
            super(itemView);

            fasting=itemView.findViewById(R.id.fasting);
            random=itemView.findViewById(R.id.random);
            date=itemView.findViewById(R.id.date);
        }
    }
}
