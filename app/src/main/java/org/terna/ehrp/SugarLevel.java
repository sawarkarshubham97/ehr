package org.terna.ehrp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.terna.ehrp.Model.SugarLevelModel;

import java.util.ArrayList;
import java.util.List;

public class SugarLevel extends AppCompatActivity {

    TextView fasting,random,date;
    Button save;
    FirebaseDatabase database;
    DatabaseReference databaseReference;

    private RecyclerView mRecyclerView;
    private SugarLevelAdapter mAdapter;
    FirebaseAuth firebaseAuth;
    private List<SugarLevelModel> mUploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugar_level);

        fasting=(TextView)findViewById(R.id.fasting);
        random=(TextView)findViewById(R.id.random);
        date=(TextView)findViewById(R.id.date);
        save=(Button)findViewById(R.id.save);

        mRecyclerView=findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mUploads = new ArrayList<>();

        FloatingActionButton fab =(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog();
            }
        });

        firebaseAuth=FirebaseAuth.getInstance();
        database=FirebaseDatabase.getInstance();

        databaseReference = database.getReference("Patient").child(firebaseAuth.getCurrentUser().getUid()).child("Sugar Level");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    SugarLevelModel sugarLevelModel =postSnapshot.getValue(SugarLevelModel.class);
                    sugarLevelModel.setKey(postSnapshot.getKey());
                    mUploads.add(sugarLevelModel);

                }

                mAdapter=new SugarLevelAdapter(SugarLevel.this,mUploads);

                mRecyclerView.setAdapter(mAdapter);



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(SugarLevel.this,databaseError.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void showDialog() {

        AlertDialog.Builder alertDialog= new AlertDialog.Builder(SugarLevel.this);


        LayoutInflater inflater = this.getLayoutInflater();
        View add_record = inflater.inflate(R.layout.sugar_level,null);

        fasting=add_record.findViewById(R.id.fasting);
        random=add_record.findViewById(R.id.random);
        date=add_record.findViewById(R.id.date);
        save=add_record.findViewById(R.id.save);

        databaseReference = database.getReference("Patient").child(firebaseAuth.getCurrentUser().getUid()).child("Sugar Level");


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SugarLevelModel sugarLevelModel= new SugarLevelModel(fasting.getText().toString(),random.getText().toString(),date.getText().toString());


                String uploadId = databaseReference.push().getKey();
                databaseReference.child(uploadId).setValue(sugarLevelModel);

                finish();


            }
        });
        alertDialog.setView(add_record);
        alertDialog.show();



    }
}
