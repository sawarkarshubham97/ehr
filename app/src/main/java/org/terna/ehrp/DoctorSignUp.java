package org.terna.ehrp;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.terna.ehrp.Model.Doctor;

public class DoctorSignUp extends AppCompatActivity {

    EditText dname,password,phonenumber,specialization,hospitalname,daddress;
    Button dsignup,dalready;

    FirebaseDatabase firebaseDatabase;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_sign_up);

        dname = (MaterialEditText) findViewById(R.id.dname);
        password = (MaterialEditText) findViewById(R.id.password);
        phonenumber = (MaterialEditText) findViewById(R.id.phonenumber);
        specialization = (MaterialEditText) findViewById(R.id.specialization);
        hospitalname = (MaterialEditText) findViewById(R.id.hospitalname);
        daddress = (MaterialEditText) findViewById(R.id.daddress);

        dsignup= (Button) findViewById(R.id.dsignup);
        dalready = (Button) findViewById(R.id.dalready);


        firebaseDatabase = FirebaseDatabase.getInstance();

        final DatabaseReference databaseReference = firebaseDatabase.getReference("Doctor");

        dsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog mDialog = new ProgressDialog(DoctorSignUp.this);
                mDialog.setMessage("Please Wait..");
                mDialog.show();

                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.child(phonenumber.getText().toString()).exists()){
                            mDialog.dismiss();
                            Toast.makeText(DoctorSignUp.this, "Phone Number already registered", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Doctor user = new Doctor(dname.getText().toString(),password.getText().toString(),phonenumber.getText().toString(),specialization.getText().toString(),hospitalname.getText().toString(),daddress.getText().toString());
                            databaseReference.child(phonenumber.getText().toString()).child("Doctor Profile").setValue(user);
                            mDialog.dismiss();
                            Toast.makeText(DoctorSignUp.this, "Sign In Successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(DoctorSignUp.this, "Please Check Some Credentials", Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });



    }
}
