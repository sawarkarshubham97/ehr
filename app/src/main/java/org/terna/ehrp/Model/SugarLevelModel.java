package org.terna.ehrp.Model;

public class SugarLevelModel {

    public String fasting;
    public String random;
    public String date;
    public String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public SugarLevelModel() {
    }

    public SugarLevelModel(String fasting, String random, String date) {
        this.fasting = fasting;
        this.random = random;
        this.date = date;
    }

    public String getFasting() {
        return fasting;
    }

    public void setFasting(String fasting) {
        this.fasting = fasting;
    }

    public String getRandom() {
        return random;
    }

    public void setRandom(String random) {
        this.random = random;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
