package org.terna.ehrp.Model;

public class Patient {
    public String pname;
    public String pemail;
    public String phone;
    public String page;
    public String pgender;
    public String paddress;

    public Patient() {
    }

    public Patient(String pname, String pemail,  String phone, String page, String pgender, String paddress) {
        this.pname = pname;
        this.pemail = pemail;

        this.phone = phone;
        this.page = page;
        this.pgender = pgender;
        this.paddress = paddress;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPemail() {
        return pemail;
    }

    public void setPemail(String pemail) {
        this.pemail = pemail;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPgender() {
        return pgender;
    }

    public void setPgender(String pgender) {
        this.pgender = pgender;
    }

    public String getPaddress() {
        return paddress;
    }

    public void setPaddress(String paddress) {
        this.paddress = paddress;
    }
}

