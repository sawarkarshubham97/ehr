package org.terna.ehrp.Model;

public class PatientAppointmentModel {

    public String dname;
    public String hospitalname;
    public String problem;
    public long appointmentTime;
    public String appointmentPushId;
    public String doctorid;

    public PatientAppointmentModel() {
    }

    public PatientAppointmentModel(String dname, String hospitalname, String problem, long appointmentTime, String appointmentPushId, String doctorid) {
        this.dname = dname;
        this.hospitalname = hospitalname;
        this.problem = problem;
        this.appointmentTime = appointmentTime;
        this.appointmentPushId = appointmentPushId;
        this.doctorid = doctorid;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public long getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(long appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getAppointmentPushId() {
        return appointmentPushId;
    }

    public void setAppointmentPushId(String appointmentPushId) {
        this.appointmentPushId = appointmentPushId;
    }

    public String getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(String doctorid) {
        this.doctorid = doctorid;
    }
}
