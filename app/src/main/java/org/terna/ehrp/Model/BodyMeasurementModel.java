package org.terna.ehrp.Model;

public class BodyMeasurementModel {
    public String height ;
    public String weight;
    private String date;
    private String Key;

    public BodyMeasurementModel() {
    }

    public BodyMeasurementModel(String height, String weight, String date) {
        this.height = height;
        this.weight = weight;
        this.date = date;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }
}
