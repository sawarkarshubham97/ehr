package org.terna.ehrp.Model;

public class BloodPressureModel {

    public String systolic ;
    public String diastolic;
    private String date;
    private String Key;

    public BloodPressureModel() {
    }

    public BloodPressureModel(String systolic, String diastolic, String date) {
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.date = date;
    }

    public String getSystolic() {
        return systolic;
    }

    public void setSystolic(String systolic) {
        this.systolic = systolic;
    }

    public String getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(String diastolic) {
        this.diastolic = diastolic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }
}
