package org.terna.ehrp.Model;

public class DoctorAppointmentModel {


    public String pname;
    public String problem;
    public String address;
    public String age;
    public String phone;
    public String gender;
    public long appointmentTime;
    public String appointmentPushId;

    public DoctorAppointmentModel() {
    }

    public DoctorAppointmentModel(String pname, String problem, String address, String age, String phone, String gender, long appointmentTime, String appointmentPushId) {
        this.pname = pname;
        this.problem = problem;
        this.address = address;
        this.age = age;
        this.phone = phone;
        this.gender = gender;
        this.appointmentTime = appointmentTime;
        this.appointmentPushId = appointmentPushId;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(long appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getAppointmentPushId() {
        return appointmentPushId;
    }

    public void setAppointmentPushId(String appointmentPushId) {
        this.appointmentPushId = appointmentPushId;
    }
}
