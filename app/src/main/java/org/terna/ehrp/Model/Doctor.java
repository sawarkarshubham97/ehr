package org.terna.ehrp.Model;

public class Doctor {

    public String dname;
    public String password;
    public String phonenumber;
    public String specialization;
    public String hospitalname;
    public String daddress;

    public Doctor() {
    }

    public Doctor(String dname, String password, String phonenumber, String specialization, String hospitalname, String daddress) {
        this.dname = dname;
        this.password = password;
        this.phonenumber = phonenumber;
        this.specialization = specialization;
        this.hospitalname = hospitalname;
        this.daddress = daddress;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    public String getDaddress() {
        return daddress;
    }

    public void setDaddress(String daddress) {
        this.daddress = daddress;
    }
}
