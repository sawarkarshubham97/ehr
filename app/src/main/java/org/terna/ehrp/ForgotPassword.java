package org.terna.ehrp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.rengwuxian.materialedittext.MaterialEditText;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {
    EditText email;
    Button reset,back;
    FirebaseAuth firebaseAuth;
    LinearLayout linear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        email = (MaterialEditText)findViewById(R.id.email);
        reset = (Button)findViewById(R.id.reset);
        back=(Button) findViewById(R.id.back);
        linear=(LinearLayout) findViewById(R.id.linear);

        reset.setOnClickListener(this);
        back.setOnClickListener(this);

        firebaseAuth=FirebaseAuth.getInstance();



    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.back){
           startActivity(new Intent(ForgotPassword.this,MainActivity.class));
           finish();
        }
        else if(v.getId()==R.id.reset){
            resetPassword(email.getText().toString());
        }

    }

    private void resetPassword(final String email) {
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Snackbar snackbar = Snackbar.make(linear,"We have sent password to email"+email,Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                else {
                    Snackbar snackbar = Snackbar.make(linear,"Failed to sent",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });
    }
}
