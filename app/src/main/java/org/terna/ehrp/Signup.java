package org.terna.ehrp;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.terna.ehrp.Model.Patient;

public class Signup extends AppCompatActivity implements View.OnClickListener {
    EditText name, email, edtPassword, number, age, address, gender;
    Button signup, already;
    FirebaseAuth firebaseAuth;
    RelativeLayout relativeLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        name = (MaterialEditText) findViewById(R.id.name);
        email = (MaterialEditText) findViewById(R.id.email);
        edtPassword = (MaterialEditText) findViewById(R.id.edtPassword);
        number = (MaterialEditText) findViewById(R.id.number);
        age = (MaterialEditText) findViewById(R.id.age);
        address = (MaterialEditText) findViewById(R.id.address);
        gender = (MaterialEditText) findViewById(R.id.gender);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativelayout);

        signup = (Button) findViewById(R.id.signup);
        already = (Button) findViewById(R.id.already);

        signup.setOnClickListener(this);
        already.setOnClickListener(this);

        //Init Firebase
        firebaseAuth = FirebaseAuth.getInstance();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (firebaseAuth.getCurrentUser() != null) {
            //handle the already login user
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signup:
                registerUser();
                break;
        }
    }

    private void registerUser() {

        final String pname = name.getText().toString().trim();
        final String pemail = email.getText().toString().trim();
        final String password = edtPassword.getText().toString().trim();
        final String phone = number.getText().toString().trim();
        final String page = age.getText().toString().trim();
        final String paddress = address.getText().toString().trim();
        final String pgender = gender.getText().toString().trim();

        if (pname.isEmpty()) {
            name.setError(getString(R.string.input_error));
            name.requestFocus();
            return;
        }

        if (pemail.isEmpty()) {
            email.setError(getString(R.string.input_error));
            email.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(pemail).matches()) {
            email.setError(getString(R.string.input_error_email_invalid));
            email.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            edtPassword.setError(getString(R.string.input_error));
            edtPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            edtPassword.setError(getString(R.string.input_error_password_length));
            edtPassword.requestFocus();
            return;
        }

        if (phone.isEmpty()) {
            number.setError(getString(R.string.input_error));
            number.requestFocus();
            return;
        }

        if (phone.length() != 10) {
            number.setError(getString(R.string.input_error_phone_invalid));
            number.requestFocus();
            return;
        }

        if (page.isEmpty()) {
            age.setError(getString(R.string.input_error));
            age.requestFocus();
            return;
        }

        if (paddress.isEmpty()) {
            address.setError(getString(R.string.input_error));
            address.requestFocus();
            return;
        }

        if (pgender.isEmpty()) {
            gender.setError(getString(R.string.input_error));
            gender.requestFocus();
            return;
        }


        firebaseAuth.createUserWithEmailAndPassword(pemail,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {


                    Patient patient = new Patient(
                            pname,pemail,phone,page,paddress,pgender

                    );

                    FirebaseDatabase.getInstance().getReference("Patient").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Patient Profile").setValue(patient).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {


                            if (task.isSuccessful()) {

                                Snackbar snackbar = Snackbar.make(relativeLayout,"Registration Successfully ",Snackbar.LENGTH_SHORT);
                                snackbar.show();


                            } else {

                                //display a failure message
                                Snackbar snackbar = Snackbar.make(relativeLayout,"Registration UnSuccessfully ",Snackbar.LENGTH_SHORT);
                                snackbar.show();
                            }

                        }
                    });
                }
                else {
                    Toast.makeText(Signup.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}







